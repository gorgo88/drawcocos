#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include <Model.h>
#include <View.h>
#include <Controller.h>


class HelloWorld : public cocos2d::Layer
{
    Model *m_model = nullptr;
    View *m_view = nullptr;
    Controller *m_controller = nullptr;

public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld)

    virtual ~HelloWorld();
};

#endif // __HELLOWORLD_SCENE_H__
