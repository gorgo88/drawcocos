//
// Created by gorgo on 15.11.16.
//

#ifndef MYGAME_CONTROLLER_H
#define MYGAME_CONTROLLER_H


#include <Model.h>
#include <View.h>

class Controller {
public:
    Controller(Model *model, View *view);

private:
    Model *m_model;
    View *m_view;
};


#endif //MYGAME_CONTROLLER_H
