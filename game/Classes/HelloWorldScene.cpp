#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;



Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    m_model = new Model(5.0f, 20.0f, Color4B::ORANGE, Color4B::BLACK);
    m_view = new View(m_model, this);
    m_controller = new Controller(m_model, m_view);
    
    return true;
}

HelloWorld::~HelloWorld() {
    if (m_view) delete m_view;
    if (m_model) delete m_model;
    if (m_controller) delete m_controller;
}
