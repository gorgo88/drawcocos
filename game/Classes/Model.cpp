//
// Created by gorgo on 15.11.16.
//

#include "Model.h"

void Model::setCursorPoint(const Vec2 &cursorPoint) {
    m_cursorPoint = cursorPoint;
    if (onCursorPointChanged) onCursorPointChanged();
}

void Model::setDrawing(bool drawing) {
    m_isDrawing = drawing;
    if (m_isDrawing && onStartDrawing) onStartDrawing();
}



