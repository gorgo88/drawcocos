//
// Created by gorgo on 15.11.16.
//

#ifndef MYGAME_MODEL_H
#define MYGAME_MODEL_H

#include <cocos2d.h>

USING_NS_CC;

class Model {


public:

    Model(float rate, float size, const Color4B &color, const Color4B &clearColor)
            : m_rate(rate),
              m_size(size),
              m_color(color),
              m_clearColor(clearColor),
              m_isDrawing(false)
              {}

    const Vec2 &getCursorPoint() const {
        return m_cursorPoint;
    }
    float getRate() const {return m_rate;}

    float getSize() const {return m_size;}

    const Color4B &getColor() const {return m_color;}
    const Color4B &getClearColor() const {return m_clearColor;}
    void setCursorPoint(const Vec2 &cursorPoint);


    std::function<void()> onCursorPointChanged = nullptr;
    std::function<void()> onStartDrawing = nullptr;
    std::function<void()> onSettingsChanged = nullptr;

    bool isDrawing() const {
        return m_isDrawing;
    }

    void setDrawing(bool drawing);

private:
    float m_rate;
    float m_size;
    Vec2 m_cursorPoint;
    Color4B m_color;
    Color4B m_clearColor;
    bool m_isDrawing;

};


#endif //MYGAME_MODEL_H
