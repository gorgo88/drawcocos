//
// Created by gorgo on 15.11.16.
//

#include "Controller.h"

Controller::Controller(Model *model, View *view)
        :   m_model(model),
            m_view(view)
{
    m_view->onInputMoved = [this](Vec2 point){
        m_model->setCursorPoint(point);
    };
    m_view->onInputActionStarted = [this](Vec2 point) {
        m_model->setCursorPoint(point);
        m_model->setDrawing(true);
    };
    m_view->onInputActionEnded = [this]() {
        m_model->setDrawing(false);
    };
}
