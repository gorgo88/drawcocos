//
// Created by gorgo on 15.11.16.
//

#ifndef MYGAME_VIEW_H
#define MYGAME_VIEW_H

#include <cocos2d.h>
#include <Model.h>

USING_NS_CC;

class View {
public:
    View(Model *model, Node *parent);

    virtual ~View();


private:
    Model *m_model;
    Node *m_node;
    Sprite *m_cursor;
    RenderTexture *m_renderTexture;
    Vec2 m_prevCursorPoint;

public:

    std::function<void(Vec2 point)> onInputActionStarted;
    std::function<void()> onInputActionEnded;
    std::function<void(Vec2 point)> onInputMoved;

    void update(float dt);
    void drawSegment();

    void updateSettings();
};


#endif //MYGAME_VIEW_H
