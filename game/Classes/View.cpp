//
// Created by gorgo on 15.11.16.
//

#include "View.h"
#include <ui/CocosGUI.h>


View::View(Model *model, Node *parent) : m_model(model) {
    m_node = Node::create();
    parent->addChild(m_node);

    auto winSize = Director::getInstance()->getWinSize(); // don't use getWinSizeInPixels here)
    m_renderTexture = RenderTexture::create(
            (int)winSize.width, (int)winSize.height, Texture2D::PixelFormat::RGBA8888);
    m_renderTexture->retain();
    m_renderTexture->getSprite()->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    m_renderTexture->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    m_renderTexture->getSprite()->setBlendFunc(BlendFunc::ALPHA_NON_PREMULTIPLIED);
    m_node->addChild(m_renderTexture, -1);

    m_cursor = Sprite::create("falloff.png");
    m_cursor->setVisible(false);
    m_node->addChild(m_cursor);

    auto listener = EventListenerTouchOneByOne::create();

    listener->onTouchBegan = [this](Touch *touch, Event *event) {
        {
            if (onInputActionStarted)
                onInputActionStarted(m_node->convertTouchToNodeSpace(touch));
            if (onInputMoved)
                onInputMoved(m_node->convertTouchToNodeSpace(touch));
        }
        return true;
    };
    listener->onTouchMoved = [this](Touch *touch, Event *event) {
        if (onInputMoved)
            onInputMoved(m_node->convertTouchToNodeSpace(touch));
    };
    listener->onTouchEnded = [this](Touch *touch, Event *event) {
        if (onInputMoved)
            onInputMoved(m_node->convertTouchToNodeSpace(touch));
        if (onInputActionEnded)
            onInputActionEnded();
    };

    Director::getInstance()->getEventDispatcher()->
            addEventListenerWithSceneGraphPriority(listener, m_renderTexture->getSprite());


    m_model->onStartDrawing = [this](){
        m_prevCursorPoint = m_model->getCursorPoint();
    };
    m_model->onSettingsChanged = [this]() { updateSettings(); };
    updateSettings();

    Director::getInstance()->getScheduler()->scheduleUpdate<View>(this, 0, false);


    // clear button
    ui::Button *btn = ui::Button::create();
    btn->loadTextureNormal("clearBtn.png");
    m_node->addChild(btn);
    btn->addClickEventListener([this](Ref* target){
        auto fCol = Color4F(m_model->getClearColor());
        m_renderTexture->clear(fCol.r, fCol.g, fCol.b, fCol.a);
    });
    btn->setPosition(Vec2(100,100));

}

void View::updateSettings() {
    m_cursor->setScale(m_model->getSize() / m_cursor->getContentSize().width);
    m_cursor->setColor(Color3B(m_model->getColor()));
    m_cursor->setOpacity(m_model->getColor().a);
}

void View::update(float dt) {
    Vec2 currentPoint = m_model->getCursorPoint();
    if (m_model->isDrawing() && ! currentPoint.equals(m_prevCursorPoint)) {
        drawSegment();
    }
    m_cursor->setPosition(currentPoint);
    m_prevCursorPoint = currentPoint;
}

// draw even distributed sprites from last pos exclusive to current pos inclusive
void View::drawSegment() {
    m_cursor->setVisible(true);

    Vec2 startPoint = m_prevCursorPoint;
    Vec2 endPoint = m_model->getCursorPoint();
    Vec2 diff = endPoint - startPoint;

    float len = diff.length();
    int iterations = (int)(len / m_model->getRate()) + 1;
    Vec2 delta = diff / iterations;

    for (int i = 0; i < iterations; ++i) {
        m_renderTexture->begin();
        Vec2 pos = endPoint - i*delta;
        m_cursor->setPosition(pos);
        m_cursor->visit();
        m_renderTexture->end();
        Director::getInstance()->getRenderer()->render(); // that's maybe not so good,
        // should dig deeper into cocos to fix this
    }

    m_cursor->setVisible(false);
}

View::~View() {
    m_renderTexture->release();
}
